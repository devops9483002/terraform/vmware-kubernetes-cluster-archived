# VMware Virtual Machine

## Using this module

```bash
export GITLAB_TOKEN="<gitlab_token>"
```

```hcl
terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}

provider "gitlab" {
  base_url = "https://gitlab.com/api/v4/"
}

provider "vsphere" {
  user                 = var.vsphere_username
  password             = var.vsphere_password
  vsphere_server       = var.vsphere_server_ip
  allow_unverified_ssl = true
}

module "k8s_cluster" {
  source = "gitlab.com/terraform2584837/vmware-kubernetes-cluster/vmware"
  version = "~> 0.11.0"

  cluster_name = "<NAME_PREFIX>"
  vm_template  = "<VM_TEMPLATE_NAME>"
}
```
