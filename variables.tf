variable "vsphere_datacenter" {
  type    = string
  default = "Datacenter"
}

variable "vm_template" {
  type        = string
  description = "The name of the template to use for the virtual machines."
}

variable "cluster_name" {
  type        = string
  description = "The name of the cluster."
}

########################################
# Master
########################################

variable "master_datastores" {
  type        = list(string)
  default     = []
  description = "The datastores to use for the master nodes."

  validation {
    condition     = length(var.master_datastores) >= 1
    error_message = "At least one datastore must be specified."
  }
}

variable "master_cores" {
  type        = number
  default     = 4
  description = "The number of cores to allocate to each master node."

  validation {
    condition     = var.master_cores >= 4 && var.master_cores <= 8
    error_message = "The number of cores must be between 4 and 8."
  }
}

variable "master_memory" {
  type        = number
  default     = 4096
  description = "The amount of memory to allocate to each master node."

  validation {
    condition     = var.master_memory >= 4096 && var.master_memory <= 16384
    error_message = "The amount of memory must be between 4096 and 16384."
  }
}

variable "master_root_disk_size" {
  type        = number
  default     = 25
  description = "The size of the root disk to allocate to each master node."

  validation {
    condition     = var.master_root_disk_size >= 25 && var.master_root_disk_size <= 1000
    error_message = "The size of the root disk must be between 25 and 1000."
  }
}

variable "master_additional_disk_size" {
  type        = number
  default     = null
  description = "The size of the additional disk to allocate to each master node."
}

########################################
# Worker
########################################

variable "worker_datastores" {
  type        = list(string)
  default     = []
  description = "The datastores to use for the worker nodes."

  validation {
    condition     = length(var.worker_datastores) >= 1
    error_message = "At least one datastore must be specified."
  }
}

variable "worker_cores" {
  type        = number
  default     = 4
  description = "The number of cores to allocate to each worker node."

  validation {
    condition     = var.worker_cores >= 4 && var.worker_cores <= 12
    error_message = "The number of cores must be between 4 and 12."
  }
}

variable "worker_memory" {
  type        = number
  default     = 4096
  description = "The amount of memory to allocate to each worker node."

  validation {
    condition     = var.worker_memory >= 4096 && var.worker_memory <= 32768
    error_message = "The amount of memory must be between 4096 and 32768."
  }
}

variable "worker_root_disk_size" {
  type        = number
  default     = 25
  description = "The size of the root disk to allocate to each worker node."

  validation {
    condition     = var.worker_root_disk_size >= 25 && var.worker_root_disk_size <= 1000
    error_message = "The size of the root disk must be between 25 and 1000."
  }
}

variable "worker_additional_disk_size" {
  type        = number
  default     = null
  description = "The size of the additional disk to allocate to each worker node."
}
